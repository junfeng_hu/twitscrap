'''
Usage:
    twitscrape_cli <username> <scrap> <dir>
    <scrap> ['all', "tweets", "followers"]
    <dir> must exists


'''

import sys

import os.path
import getpass
from lib import Twitter
from lib import genexcel


def print_tweets(gentweets, imagedir, scraper):
    i = 1
    for tweets in gentweets:
        for t in tweets:
            print ("-"*50)
            print t
            print ("***attention: %d***" % i)
            img_url = t.get("image")
            imgfile = img_url.rsplit('/', 1)[-1].rsplit(':', 1)[0] if img_url.endswith(":large") else img_url.rsplit('/', 1)[-1]
            imgfile= "_".join((t["tweet_id"], t["time"], imgfile))
            if os.path.isfile(os.path.join(imagedir, imgfile)):
                continue
            r = scraper.session.get(img_url)
            with open(os.path.join(imagedir,imgfile), "wb") as fd:
                for chunk in r.iter_content(4096):
                    fd.write(chunk)
                print ("downloads %s successfully" % imgfile)

            i += 1

def print_followers(genfollowers, photodir, scraper):
    i = 1
    for followers in genfollowers:
        for u in followers:
            print ("-"*50)
            print u
            avatar_url = u.get("avatar")
            if avatar_url:
                avatar_file = u["screen_name"] + "." + avatar_url.rsplit(".", 1)[-1]
                if os.path.isfile(os.path.join(photodir, avatar_file)):
                    continue
                r = scraper.session.get(avatar_url)
                with open(os.path.join(photodir,avatar_file), "wb") as fd:
                    for chunk in r.iter_content(4096):
                        fd.write(chunk)
                    print ("downloads %s successfully" % avatar_file)
            print ("***attention: %d***" % i)
            i += 1


if __name__=="__main__":
    arguments = sys.argv[1:]
    if len(arguments) != 3:
        print __doc__
        sys.exit(0)

    if "-h" == arguments[0] or "--help" == arguments[0]:
        print __doc__
        sys.exit(0)
    else:
        try:
            username = arguments[0]
            scrap = arguments[1]
            output_dir = arguments[2]
        except Exception, e:
            print __doc__
            sys.exit(0)

        if scrap not in ['all', "tweets", "followers","followings"]:
            print __doc__
            sys.exit(0)
        if  not os.path.isdir(output_dir):
            print __doc__
            sys.exit(0)
        loginname= raw_input("username or email login to Twitter:")
        password = getpass.getpass()
        imagedir = os.path.join(output_dir, username)
        if not os.path.isdir(imagedir):
            os.mkdir(imagedir)

        photodir = os.path.join(imagedir, "photos")
        if not os.path.isdir(photodir):
            os.mkdir(photodir)

        scraper= Twitter.ScrapTweets(username, loginname, password)
        excel = genexcel.GenExcel(username, output_dir)
        if scrap == "tweets":
            gentweets = scraper.gentweets()
            excel.tweets2excel(gentweets)
            excel.save()
        elif scrap == "followers":
            genfollowers = scraper.genfollowers()
            excel.followers2excel(genfollowers, photodir)
            excel.save()
        elif scrap == "all":
            gentweets = scraper.gentweets()
            excel.tweets2excel(gentweets)
            genfollowers = scraper.genfollowers()
            excel.followers2excel(genfollowers, photodir)
            excel.save()
        else:
            print("nothing to do.")







