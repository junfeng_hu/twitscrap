# coding: utf-8

import json
import HTMLParser

import requests
import bs4


TWITTER_URL = "https://twitter.com"

UPDATE_TIMELINE = TWITTER_URL + "/i/profiles/show/%s/timeline?include_available_features=1&include_entities=1&max_id=%s"

RETWEETED_USERS_URL = TWITTER_URL + "/i/activity/retweeted_popup"


html_parser = HTMLParser.HTMLParser()


class ScrapTweets(object):

    def __init__(self, username, loginname, password, proxies=None):
        "username: homepage url name."
        self.username = username
        self.loginname = loginname
        self.password = password
        self.homepage = TWITTER_URL + "/" + username
        self.proxies = proxies
        self.session = None
        self.max_id = "-1"
        self.tweet_stats = 0
        self.following_stats = 0
        self.follower_stats = 0

        self.fetched_tweet_stats = 0
        self.fetched_following_stats = 0
        self.fetched_follower_stats = 0

        self.failure_tweet_stats = 0
        self.failure_following_stats = 0
        self.failure_follower_stats = 0


        #followers
        self.cursor = 0
        self.login()


    def login(self):
        "create session and login"
        self.session = requests.Session()
        if self.proxies:
            self.session.proxies=proxies
        #self.session.verify=False

        login_post = "https://twitter.com/sessions"
        login_get = "https://twitter.com/login"
        r = self.session.get(login_get)
        soup = bs4.BeautifulSoup(r.text)
        loginform = soup.find("form", class_="clearfix signin js-signin")
        inputs = loginform.find_all("input")
        data=[]
        for input in inputs:
            name = input.get("name")
            if name:
                if "username" in name:
                    value = self.loginname
                elif "password" in name:
                    value = self.password
                else:
                    value = input.get("value")
                data.append((name,value))

        r=self.session.post(login_post, data=data)
        if r.url != "https://twitter.com/" or r.status_code != 200:
            raise Exception("login failure of loginname:%s" % self.loginname)

    def gentweets(self):
        "fetch user's all tweets. return a generator."

        yield self.get_homepage()

        while True:
            url = UPDATE_TIMELINE % (self.username, self.max_id)
            try:
                r = self.session.get(url)
            except Exception, e:
                print url
                print e
            ordinary_str = r.text
            html_json = json.loads(ordinary_str.replace("\/", "/"))
            if html_json['max_id'] == '-1' and not html_json["has_more_items"]:
                '''
                if self.fetched_tweet_stats < self.tweet_stats:
                    print html_json
                    print("see max_id == 1 but not fetch all tweets")
                    self.session.close()
                    self.login()
                    continue
                '''
                print("successfully fetched all tweets.")
                break
            if html_json["max_id"] != "-1":

                self.max_id = html_json["max_id"]
            twsoup = bs4.BeautifulSoup(html_json["items_html"], "lxml")
            try:
                lis=twsoup.body.find_all("li",recursive=False)
            except Exception, e:
                print (r.url, self.max_id)
                continue
            tweets = []
            for li in lis:
                tweets.append(self._li2tweetdict(li))
            yield tweets







    def get_homepage(self):
        "request homepage, return list of tweets"
        r = self.session.get(self.homepage)
        if r.status_code == 404:
            raise ("not find %s. username must screen_name" % r.url)
        soup = bs4.BeautifulSoup(r.text, "lxml")

        #following_stats,tweet_stats,follower_stats
        profile_stats_ul = soup.find("ul", class_="stats js-mini-profile-stats ")
        try:
            self.tweet_stats = int(profile_stats_ul.find("a", attrs={"data-element-term":"tweet_stats"}).find("strong").get("title", "0").replace(',', ''))
        except Exception, e:
            pass
        try:
            self.following_stats = int(profile_stats_ul.find("a", attrs={"data-element-term":"following_stats"}).find("strong").get("title", "0").replace(',', ''))
        except Exception, e:
            pass
        try:
            self.follower_stats = int(profile_stats_ul.find("a", attrs={"data-element-term":"follower_stats"}).find("strong").get("title", "0").replace(',', ''))
        except Exception, e:
            pass


        print ("url:%s\ntweets:%d\nfollowers:%d\nfollowerings:%d" %(self.homepage, self.tweet_stats,
            self.follower_stats, self.following_stats))
        print ("start scrape tweets...")
        ol = soup.find("ol", id="stream-items-id")
        stream_container_div = ol.parent.parent
        self.max_id = stream_container_div.get("data-max-id", "-1")

        lis = ol.find_all("li",recursive=False)
        tweets = []
        for li in lis:
            try:
                tweets.append(self._li2tweetdict(li))
            except Exception, e:
                print e
        return tweets



    def _li2tweetdict(self, li):
        "li element to tweet dict"
        div = li.find('div')
        t={}
        t['tweet_id'] = div.get("data-tweet-id")
        t['retweet_id'] = div.get("data-retweet-id", "-1")
        t['name'] = div.get("data-name", '')
        t['screen_name'] = div.get("data-screen-name")
        t['user_id'] = div.get("data-user-id")
        content = div.find(class_="content")
        t['time'] = content.find(
                class_="stream-item-header"
                ).find("small").find("span").get("data-time")
        #text = ''.join(content.select(".js-tweet-text.tweet-text")[0].strings).replace('\n', ' ')
        text = ''.join(content.select(".js-tweet-text.tweet-text")[0].strings)
        t['content'] = text
        image = ""

        media_container = content.find("div", class_="cards-media-container js-media-container")
        if media_container:
            pass
        else:
            imgsoup = bs4.BeautifulSoup(html_parser.unescape(div.get("data-expanded-footer", '')), "lxml")

            media_container = imgsoup.find("div", class_="cards-media-container js-media-container")
        imga = media_container.find("a", class_="media") if media_container else None
        if imga:
            image = imga.get("data-url", '')
        t['image'] = image


        retweeted_count = '0'
        retweeted_users = []
        params = {"id":t["tweet_id"]}
        try:
            r=self.session.get(RETWEETED_USERS_URL, params=params)
            retweeted_json = json.loads(r.text.replace("\/","/"))
            retweeted_count = retweeted_json["htmlTitle"].split(" ")[1].replace(",", "")
            htmlusers = retweeted_json["htmlUsers"]
            rtsoup = bs4.BeautifulSoup(htmlusers, "lxml")
            userlis = rtsoup.find("ol").find_all("li", recursive=False)
            for uli in userlis:
                user = {}
                udiv = uli.select("div.account.js-actionable-user.js-profile-popup-actionable")[0]
                user["screen_name"] = udiv.get('data-screen-name')
                user["user_id"] = udiv.get('data-user-id')
                retweeted_users.append(user)
        except Exception, e:
            print e
            print ("tweet_id %s" %(t["tweet_id"]))
        t["retweeted_count"] = retweeted_count
        t["retweeted_users"] = retweeted_users

        self.fetched_tweet_stats += 1
        return t


    def genfollowers(self):
        "return generators of followers"
        FOLLOWERS_URL = "https://twitter.com/%s/followers/users?cursor=%s&cursor_index=&cursor_offset=&include_available_features=1&include_entities=1&is_forward=true"
        yield self.get_followers_page()

        while True:
            url = FOLLOWERS_URL % (self.username, self.cursor)
            try:
                r = self.session.get(url)
            except Exception, e:
                print url
                print e
                continue
            ordinary_str = r.text
            html_json = json.loads(ordinary_str.replace("\/", "/"))
            if html_json['cursor'] == '0' and not html_json["has_more_items"]:
                print("successfully fetched all followers.")
                break
            if html_json["cursor"] != "0":

                self.cursor = html_json["cursor"]
            twsoup = bs4.BeautifulSoup(html_json["items_html"], "lxml")
            try:
                lis=twsoup.body.find_all("li",recursive=False)
            except Exception, e:
                print (r.url, self.cursor)
                continue
            followers = []
            for li in lis:
                followers.append(self._li2usersdict(li))
            yield followers

    def get_followers_page(self):
        "request homepage, return list of tweets"
        r = self.session.get(self.homepage + "/followers")
        print("start scrape followers...")
        soup = bs4.BeautifulSoup(r.text, "lxml")
        ol = soup.find("ol", id="stream-items-id") # user followers ol
        self.cursor = ol.parent.parent.get("data-cursor","0")

        lis = ol.find_all("li",recursive=False)
        followers = []
        for li in lis:
            try:
                followers.append(self._li2usersdict(li))
            except Exception, e:
                print e
        return followers


    def _li2usersdict(self, li):
        "li element to users dict"
        div = li.find('div')
        u={}
        u["user_id"] = div.get("data-user-id")
        u["screen_name"] = div.get("data-screen-name")
        content = div.find("div", class_="content")
        img = content.find("img", class_="avatar js-action-profile-avatar ")
        u["avatar"] = img.get("src", "") if img else ""
        bio = content.find("p", class_="bio")
        u["bio"] = "".join(bio.strings).replace("\n", " ") if bio else ""
        return u















