#!---coding:utf-8---

import os.path
import os
from datetime import datetime
import json

import xlsxwriter
import requests
from Twitter import TWITTER_URL

STATUS_URL = TWITTER_URL + "/%s/status/%s"
class GenExcel(object):
    def __init__(self,username, output_dir):
        self.path = os.path.join(output_dir, username + ".xlsx")
        self.username = username
        if os.path.isfile(self.path):
            "remove exists file"
            os.remove(self.path)

        self.workbook = xlsxwriter.Workbook(self.path)

    def tweets2excel(self, gentweets):
        "writing tweets to excel sheet"
        headers = ["url", "time", "content", "images_url", "retweet", "retweeted_count", "reweeted_users"]
        date_format = self.workbook.add_format({'num_format': 'mmm d yyyy hh:mm AM/PM'})
        bold = self.workbook.add_format({'bold': True})
        bold.set_align("center")
        center = self.workbook.add_format()
        center.set_align("center")
        center.set_align("vjustify")

        worksheet = self.workbook.add_worksheet("Tweets")
        worksheet.set_column(0, 1, 20)
        worksheet.set_column(2, 2, 100)
        worksheet.set_column(3, 3, 30)
        worksheet.set_column(5, 6, 20)

        row = 0
        col = 0
        for h in headers:
            worksheet.write(row, col, h, bold)
            col += 1
        row += 1
        col = 0
        for tweets in gentweets:
            for t in tweets:
                print ("got %d tweets." % row)
                try:
                    status_url = STATUS_URL %(t["screen_name"], t["tweet_id"])
                    worksheet.write_url(row, col, status_url, string=t["tweet_id"])

                    date_time = datetime.fromtimestamp(float(t["time"]))
                    worksheet.write_datetime(row, col+1, date_time, date_format)
                    content = t["content"]
                    worksheet.write_string(row, col+2, content, center)
                    image_url = t["image"]
                    worksheet.write_url(row, col+3, image_url)


                    if t["retweet_id"] == "-1":
                        retweeted = "no"
                    else:
                        retweeted = "yes"
                    worksheet.write_string(row, col+4, retweeted)

                    retweeted_count = t["retweeted_count"]
                    if t["retweeted_count"] != "0":
                        retweeted_users = json.dumps(t["retweeted_users"])
                    else:
                        retweeted_users = "no retweeted users."
                    worksheet.write_number(row, col+5, float(retweeted_count))
                    worksheet.write_string(row, col+6, retweeted_users)

                    row += 1
                except Exception, e:
                    print t
                    print e





    def followers2excel(self, genfollowers, photodir):
        "writing followers to excel sheet"
        worksheet = self.workbook.add_worksheet("Followers")
        headers = ["user_id", "screen_name", "avatar", "bio"]
        bold = self.workbook.add_format({'bold': True})
        bold.set_align("center")
        center = self.workbook.add_format()
        center.set_align("center")
        center.set_align("vjustify")

        worksheet.set_column(0, 2, 20)
        worksheet.set_column(3, 3, 100)

        row = 0
        col = 0
        for h in headers:
            worksheet.write(row, col, h, bold)
            col += 1
        row += 1
        col = 0
        for followers in genfollowers:
            for u in followers:
                print ("got %d followers." % row)
                try:
                    user_url = TWITTER_URL + "/" + u["screen_name"]
                    worksheet.write_url(row, col, user_url, string=u["user_id"])

                    screen_name = u["screen_name"]
                    worksheet.write_string(row, col+1, screen_name, center)
                    avatar_url = u["avatar"]
                    if avatar_url != "" and not avatar_url.endswith((".gif", ".GIF")):
                        try:
                            avatar_file = u["screen_name"] + "." + avatar_url.rsplit(".", 1)[-1]
                            avatar_file = avatar_file.replace("/", "")
                            if not os.path.isfile(os.path.join(photodir, avatar_file)):
                                r = requests.get(avatar_url)
                                with open(os.path.join(photodir,avatar_file), "wb") as fd:
                                    for chunk in r.iter_content(4096):
                                        fd.write(chunk)
                                    print ("downloads %s successfully" % avatar_file)
                            self.workbook._get_image_properties(os.path.join(photodir, avatar_file))
                            worksheet.insert_image(row, col+2, os.path.join(photodir, avatar_file),{"url":avatar_url})
                        except Exception, e:
                            print e
                            print "failured insert %s at row %d" %(avatar_file, row+1)
                            worksheet.write_url(row, col+2, avatar_url, center)
                    else:
                        worksheet.write_url(row, col+2, avatar_url, center)
                    worksheet.write_string(row, col+3, u["bio"], center)
                    worksheet.set_row(row, 50)
                    row += 1
                except Exception, e:
                    print u
                    print e

    def save(self):
        self.workbook.close()
        print ("successfully output excel file.")

